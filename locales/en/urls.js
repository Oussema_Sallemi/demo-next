const urls = [
  { translated: "shoes", original: "chaussures-de-sport" },
  { translated: "clothing", original: "vetements-sport" },
  { translated: "sports-electronics", original: "electronique" },
  { translated: "bike", original: "velos" },
  { translated: "nutrition", original: "nutrition-sportive" },
  { translated: "running-trail-products", original: "running" },
  { translated: "cycling-products", original: "velos" },
  { translated: "walking-products", original: "univers-randonnee" },
  { translated: "fitness-products", original: "fitness" },
  { translated: "swimming-products", original: "natation" },
  { translated: "lepape-promos", original: "promos" },
];
module.exports = urls;
