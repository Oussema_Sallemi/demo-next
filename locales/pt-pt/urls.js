const urls = [
  { translated: "shoes", original: "chaussures-de-sport" },
  { translated: "clothing", original: "vetements-sport" },
  { translated: "sports-electronics", original: "electronique" },
  { translated: "bike", original: "velos" },
  { translated: "nutrition", oroginal: "nutrition-sportive" },
  { translated: "running-trail-products", oroginal: "running" },
  { translated: "cycling-products", oroginal: "velos" },
  { translated: "walking-products", oroginal: "univers-randonnee" },
  { translated: "fitness-products", ororginal: "fitness" },
  { translated: "swimming-products", oroginal: "natation" },
  { translated: "lepape-promos", oroginal: "promos" },
];
module.exports = urls;
