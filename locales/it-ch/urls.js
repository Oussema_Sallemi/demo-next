const urls = [
  { translated: "scarpe", original: "chaussures-de-sport" },
  { translated: "vestiti", original: "vetements-sport" },
  { translated: "elettronica", original: "electronique" },
  { translated: "biciclette", original: "velos" },
  { translated: "nutrizione", oroginal: "nutrition-sportive" },
  { translated: "corsa-e-percorso", oroginal: "running" },
  { translated: "ciclismo", oroginal: "velos" },
  { translated: "esterno", oroginal: "univers-randonnee" },
  { translated: "attrezzatura-fitness", ororginal: "fitness" },
  { translated: "nuoto", oroginal: "natation" },
  { translated: "promozioni-lepape", oroginal: "promos" },
];
module.exports = urls;
