const urls = [
  { translated: "schuh", original: "chaussures-de-sport" },
  { translated: "kleidung", original: "vetements-sport" },
  { translated: "elektronic", original: "electronique" },
  { translated: "bikes", original: "velos" },
  { translated: "ernahrung", original: "nutrition-sportive" },
  { translated: "aufen-and-schiene", original: "running" },
  { translated: "radlust", original: "velos" },
  { translated: "aussen", original: "univers-randonnee" },
  { translated: "fitness", original: "fitness" },
  { translated: "schwimmen", original: "natation" },
  { translated: "lepape-forderung", original: "promos" },
];
module.exports = urls;
