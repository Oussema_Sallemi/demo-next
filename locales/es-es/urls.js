const urls = [
  { translated: "zapatos", original: "chaussures-de-sport" },
  { translated: "ropa", original: "vetements-sport" },
  { translated: "electronica", original: "electronique" },
  { translated: "bicicletas", original: "velos" },
  { translated: "nutricion", original: "nutrition-sportive" },
  { translated: "correr-y-caminar", original: "running" },
  { translated: "ciclismo", original: "velos" },
  { translated: "exterior", original: "univers-randonnee" },
  { translated: "equipo-de-aptitud", original: "fitness" },
  { translated: "natacion", original: "natation" },
  { translated: "promociones-de-lepape", original: "promos" },
];
module.exports = urls;
