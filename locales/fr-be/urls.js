const urls = [
  { translated: "chaussures-de-sport", original: "chaussures-de-sport" },
  { translated: "clotvetements-sporthing", original: "vetements-sport" },
  { translated: "electronique", original: "electronique" },
  { translated: "velos", original: "velos" },
  { translated: "nutrition-sportive", original: "nutrition-sportive" },
  { translated: "running", original: "running" },
  { translated: "velos", original: "velos" },
  { translated: "univers-randonnee", original: "univers-randonnee" },
  { translated: "fitness", original: "fitness" },
  { translated: "natation", original: "natation" },
  { translated: "promos", original: "promos" },
];
module.exports = urls;
