import axios from "axios";
import { Fragment, useState, useEffect } from "react";
import { useRouter } from "next/router";
import Footer from "../components/organisms/Footer/index";
import Header from "../components/organisms/Header/index";
const Main = ({ children }) => {
  const servicesMenu = [
    { content: "Livraison 24/48h offerte dès 90€ d'achat", href: "/" },
    { content: "Paiement CB sécurisé en 2,3,4 x sans frais", href: "/" },
    { content: "Echanges rapides et gratuits", href: "/" },
    { content: "Conditions générales de vente", href: "/" },
    { content: "Nos codes promo et avantages fidélité", href: "/" },
    { content: "Déstockage - plus de 40% de remise", href: "/" },
  ];
  const discoverMenu = [
    { content: "L'entreprise", href: "/" },
    { content: "LEPAPE PRO", href: "/" },
    { content: "Nos offres d'emploi", href: "/" },
    { content: "Partenaires", href: "/" },
    { content: "Programme d'affiliation", href: "/" },
    { content: "Conseils triathlon", href: "/" },
    { content: "Conseils home trainers", href: "/" },
    { content: "Idées cadeaux Vélo", href: "/" },
    { content: "Black Friday", href: "/" },
    { content: "Soldes Running & Trail", href: "/" },
    { content: "Soldes Vélo", href: "/" },
    { content: "Jeux concours", href: "/" },
  ];

  return (
    <Fragment>
      <Header menu={[]} />
      <div>
        <main>{children}</main>
      </div>
      <Footer servicesMenu={servicesMenu} discoverMenu={discoverMenu} />
    </Fragment>
  );
};

export default Main;
