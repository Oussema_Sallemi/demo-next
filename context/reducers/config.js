export function config(state, action) {
  switch (action.type) {
    case "UPDATE_STORE_CODE":
      return { ...state, locale: action.payload };
    default:
      return state;
  }
}
