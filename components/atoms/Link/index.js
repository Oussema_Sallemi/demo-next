import Link from "next/link";

const index = ({
  linkClassName,
  href,
  children,
  as,
  locale,
  aTagClassName,
  title,
  onClick,
}) => {
  return (
    <Link href={href} as={as} locale={locale} className={linkClassName}>
      <a className={aTagClassName} title={title} onClick={onClick}>
        {children}
      </a>
    </Link>
  );
};

export default index;
