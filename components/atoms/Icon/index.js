import { Fragment } from "react";

const index = ({ className, children }) => {
  return (
    <Fragment>
      <i className={className}>{children}</i>
    </Fragment>
  );
};

export default index;
