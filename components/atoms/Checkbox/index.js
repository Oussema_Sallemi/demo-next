import { Fragment } from "react";

const index = ({ className, id, checkboxName, value, required }) => {
  return (
    <Fragment>
      <input
        className={className}
        type="checkbox"
        id={id}
        name={checkboxName}
        value={value}
        required={required}
      />
    </Fragment>
  );
};

export default index;
