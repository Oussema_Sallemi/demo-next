import { Fragment } from "react";

const index = ({ className, labelFor, children, placeholder, type }) => {
  return (
    <Fragment>
      <label className={className} htmlFor={labelFor}>
        {children}
      </label>
    </Fragment>
  );
};

export default index;
