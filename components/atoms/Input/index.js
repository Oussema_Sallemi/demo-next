import { Fragment } from "react";

const index = ({ className, id, inputName, placeholder, type }) => {
  return (
    <Fragment>
      <input
        className={className}
        type={type}
        id={id}
        name={inputName}
        placeholder={placeholder}
      />
    </Fragment>
  );
};

export default index;
