import Image from "next/image";
const index = ({ className, alt, src, width, height }) => {
  return (
    <Image
      className={className}
      src={src}
      alt={alt}
      width={width}
      height={height}
    />
  );
};

export default index;
