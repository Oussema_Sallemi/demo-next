const index = ({
  className,
  buttonClassName,
  onClick,
  type,
  disabled,
  children,
  title,
}) => {
  return (
    <div className={className}>
      <button
        type={type}
        title={title}
        disabled={disabled}
        className={buttonClassName}
        onClick={onClick}
      >
        {children}
      </button>
    </div>
  );
};

export default index;
