import Link from "../../atoms/Link/index";
import Image from "../../atoms/Image/index";
const index = ({ imageClassName, alt, src, href, height, width }) => {
  return (
    <div className="logo">
      <h1>
        <Link href={href}>
          <Image
            src={src}
            alt={alt}
            className={imageClassName}
            height={height}
            width={width}
          />
        </Link>
      </h1>
    </div>
  );
};

export default index;
