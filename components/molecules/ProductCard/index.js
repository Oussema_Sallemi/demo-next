import { Fragment } from "react";
import Image from "../../atoms/Image/index";
import Link from "../../atoms/Link/index";
import Icon from "../../atoms/Icon/index";
import Button from "../../atoms/Button/index";
import _ from "lodash";
const index = ({
  imgSrc,
  imgClassName,
  imgAlt,
  width,
  height,
  isNew,
  price,
  href,
  rating,
  product,
  brand,
}) => {
  let stars = [];
  _.times(rating, (i) => {
    stars.push(
      <span className="yellow" key={i}>
        <Icon className="fa fa-star" />
      </span>
    );
  });
  return (
    <div className="item-product">
      <div className="product-thumb">
        <Link href={href}>
          <Image
            src={imgSrc}
            alt={imgAlt}
            className={imgClassName}
            width={width}
            height={height}
          />
        </Link>
        {isNew ? (
          <div className="box-label">
            <div className="label-product-new">
              <span>Nouveau</span>
            </div>
          </div>
        ) : (
          <Fragment />
        )}
      </div>

      <div className="product-caption">
        <div className="product-name">
          <Link href={href}>
            <b>{brand}</b>
            <span>{product}</span>
          </Link>
        </div>
        <div className="rating">{stars}</div>
        <div className="price-box">
          <span className="regular-price">{price}€</span>
        </div>
        <div className="cart">
          <Button type="submit" className="add-to-cart">
            Ajouter au panier
          </Button>
        </div>
      </div>
    </div>
  );
};

export default index;
