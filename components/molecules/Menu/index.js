import Link from "../../atoms/Link/index";
export default function index({ menu }) {
  console.log(menu);
  const display = menu.map((item) => {
    return (
      <li key={item.id}>
        <Link
          key={item.id}
          href={{
            pathname: `${item.url_key}`,
            query: {
              pathname: `${item.url_key}`,
              type: "CATEGORY",
            },
          }}
          as={`/${item.url_key}`}
        >
          {item.menu_name}
        </Link>
      </li>
    );
  });
  return (
    <div className="header-menu header-bottom-area theme-bg">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <div className="header-menu ">
              <a href="#menu" className="menu-icon">
                <i className="ico ico-burger-button"></i>
              </a>
              <nav className="main-menu" id="menu">
                <ul>{display}</ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
