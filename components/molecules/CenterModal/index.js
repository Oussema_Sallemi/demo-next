import { Modal } from "react-responsive-modal";
import "react-responsive-modal/custom.css";

export default function index({ open, onClose, children }) {
  return (
    <Modal open={open} onClose={onClose} center>
      {children}
    </Modal>
  );
}
