import Link from "../../atoms/Link/index";
import Image from "../../atoms/Image/index";
const index = ({ href, height, width, imgSrc, imgClassName, imgAlt }) => {
  return (
    <div className="single-brand">
      <Link href={href}>
        <Image
          src={imgSrc}
          alt={imgAlt}
          className={imgClassName}
          height={height}
          width={width}
        />
      </Link>
    </div>
  );
};

export default index;
