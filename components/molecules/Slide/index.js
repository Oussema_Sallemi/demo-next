import Image from "../../atoms/Image/index";
export default function index({
  src,
  height,
  width,
  imgClassName,
  alt,
  slideClassName,
}) {
  return (
    <div className={slideClassName}>
      <Image
        src={src}
        alt={alt}
        className={imgClassName}
        height={height}
        width={width}
      />
    </div>
  );
}
