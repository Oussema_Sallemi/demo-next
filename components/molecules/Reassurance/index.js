import { Fragment } from "react";
import Icon from "../../atoms/Icon/index";

export default function index({ iconClassName, title, text }) {
  return (
    <Fragment>
      <Icon className={iconClassName} />
      <h4 className="ship-title">{title}</h4>
      <p>{text}</p>
    </Fragment>
  );
}
