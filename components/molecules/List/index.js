import { Fragment } from "react";
import Link from "../../atoms/Link/index";
export default function index({
  className,
  href,
  linkClassName,
  aTagClassName,
  children,
}) {
  return (
    <Fragment>
      <li className={className}>
        <Link
          href={href}
          className={linkClassName}
          aTagClassName={aTagClassName}
          children={children}
        />
      </li>
    </Fragment>
  );
}
