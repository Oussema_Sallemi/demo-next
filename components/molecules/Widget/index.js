import React from "react";
import Image from "../../atoms/Image/index";
import Link from "../../atoms/Link/index";
import Icon from "../../atoms/Icon/index";

export default function index({
  href,
  imgSrc,
  imgAlt,
  imgClassName,
  height,
  width,
  title,
  detail,
  iconClassName,
}) {
  return (
    <React.Fragment>
      <div className="single-banner">
        <Link href={href}>
          <Image
            src={imgSrc}
            alt={imgAlt}
            className={imgClassName}
            height={height}
            width={width}
          />
        </Link>
      </div>
      <div className="box-home-in">
        <p className="title">
          <strong>{title}</strong>
        </p>
        <p className="detail">{detail}</p>
        <Icon className={iconClassName} />
      </div>
    </React.Fragment>
  );
}
