import Reassurance from "../../molecules/Reassurance/index";
export default function index({ reassurances }) {
  const { data, title } = reassurances;
  const dispaly = data.map((r) => {
    return (
      <div className={r.className} key={r.id}>
        <Reassurance
          key={r.id}
          title={r.title}
          iconClassName={r.iconClassName}
          text={r.text}
        />
      </div>
    );
  });
  return (
    <div className="all-shipping">
      <div className="container">
        <div className="row">
          <h3 className="box-title">{title}</h3>
          <div className="col-xs-12">
            <div className="shipping-content">
              <div className="row">
                <div>{dispaly}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
