import Widget from "../../molecules/Widget/index";
export default function index({ widgets }) {
  const display = widgets.map((widget) => {
    return (
      <div className={widget.className} key={widget.id}>
        <Widget
          href={widget.href}
          imgSrc={widget.imgSrc}
          imgAlt={widget.imgAlt}
          imgClassName={widget.imgClassName}
          height={widget.height}
          width={widget.width}
          title={widget.title}
          detail={widget.detail}
          iconClassName={widget.iconClassName}
        />
      </div>
    );
  });
  return (
    <div className="banner-area mt-10">
      <div className="container">
        <div className="row">{display}</div>
      </div>
    </div>
  );
}
