import List from "../../molecules/List/index";
import React from "react";

const index = ({ props }) => {
  const breadcrumbs = props.map((prop) => (
    <List
      itemClassName={prop.itemClassName}
      href={prop.href}
      linkClassName={prop.linkClassName}
      aTagClassName={prop.aTagClassName}
      children={prop.children}
      key={prop.id}
    />
  ));
  return (
    <div className="breadcrumb_content">
      <ul>{breadcrumbs}</ul>
    </div>
  );
};

export default index;
