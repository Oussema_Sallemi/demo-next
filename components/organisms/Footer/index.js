import Input from "../../atoms/Input/index";
import Checkbox from "../../atoms/Checkbox/index";
import Label from "../../atoms/Label/index";
import Link from "../../atoms/Link/index";
import Button from "../../atoms/Button/index";
import List from "../../molecules/List/index";
export default function index({ servicesMenu, discoverMenu }) {
  const services = servicesMenu.map((prop) => (
    <List href={prop.href} children={prop.content} key={prop.content} />
  ));
  const discover = discoverMenu.map((prop) => (
    <List href={prop.href} children={prop.content} key={prop.content} />
  ));
  return (
    <footer>
      <div className="footer-top">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="row">
                <div className="col-md-3 col-xs-12">
                  <div className="widgets_container">
                    <h6>DÉCOUVREZ NOS MAGASINS</h6>
                    <div className="box-footer">
                      <div className="sub-title">
                        <strong>LEPAPE</strong> STORE
                      </div>
                      <ul>
                        <li>
                          <span className="city">
                            <strong>LEPAPE</strong> STORE PARIS
                          </span>
                          <span className="street">Champs-élysées</span>
                          <Link href="#?" children="En savoir plus" />
                        </li>
                        <li>
                          <span className="city">
                            <strong>LEPAPE</strong> STORE Lyon
                          </span>
                          <span className="street">république</span>
                          <Link href="#?" children="En savoir plus" />
                        </li>
                      </ul>
                    </div>
                    <div className="box-footer">
                      <div className="sub-title">
                        <strong>EN SELLE MARCEL</strong> PARIS
                      </div>
                      <ul>
                        <li>
                          <span className="street">
                            vélos urbains, vélos pliants <br />
                            vélos tout chemin, VAE
                          </span>
                          <Link
                            className="sub-title"
                            href="#?"
                            children="Voir les magasins"
                          />
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div className="col-md-3 col-xs-12">
                  <div className="widgets_container">
                    <h6>RESTONS EN CONTACT</h6>
                    <div className="box-footer">
                      <div className="sub-title">NEWSLETTER</div>
                      <p>
                        Abonnez-vous à notre newsletter pour RECEVOIR nos
                        sélections personnalisées, nouveautés, promotions et
                        conseils
                      </p>
                      <div className="newsletter-box">
                        <form id="mc-form" className="mc-form">
                          <Label
                            lassName="adress"
                            children="Votre adresse email"
                          />
                          <Input
                            type="email"
                            id="mail"
                            className="email-box"
                            placeholder="Email..."
                            name="mail"
                          />

                          <div className="form-check">
                            <div
                              className="custom-checkbox"
                              role="checkbox"
                              aria-checked="false"
                            >
                              <Checkbox
                                className="form-check-input"
                                type="checkbox"
                                id="news"
                              />
                              <span className="checkmark"></span>
                              <Label
                                className="form-check-label"
                                htmlFor="news"
                              >
                                {" "}
                                Je souhaite recevoir la newsletter de Lepape par
                                e-mail. Pour plus d'informations, consulter
                                notre{" "}
                                <Link
                                  href="#?"
                                  title="Politique de confidentialité"
                                >
                                  Politique de confidentialité
                                </Link>{" "}
                              </Label>
                            </div>
                          </div>
                          <Button
                            id="mc-submit"
                            className="newsletter-btn"
                            type="submit"
                          >
                            Je m'abonne
                          </Button>
                        </form>
                      </div>
                      <div className="sub-title">SUR LES RÉSEAUX SOCIAUX</div>
                    </div>
                  </div>
                </div>
                <div className="col-md-3 col-xs-12">
                  <div className="widgets_container">
                    <h6>PROFITEZ DE NOS SERVICES</h6>
                    <div className="footer_menu">
                      <ul>{services}</ul>
                    </div>
                  </div>
                </div>
                <div className="col-md-3 col-xs-12">
                  <div className="widgets_container space-l">
                    <h6>DÉCOUVRIR</h6>
                    <div className="footer_menu">
                      <ul>{discover}</ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="footer-bottom">
        <div className="container">
          <div className="row">
            <div className=" col-xs-12">
              <div className="footer-bottom-content">
                <div className="footer-copyright text-center">
                  <p>
                    © Copyright LEPAPE 1996-2020 | 39, rue d´Artois - 75008
                    Paris
                    <br />
                    Tel. : 01 53 75 00 00 | info@lepape.com 439 656 976 R.C.S.
                    Paris Au capital de 104 246.20 € N° TVA intracommunautaire :
                    FR54439656976
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}
