import Logo from "../../molecules/Logo/index";
import Menu from "../../molecules/Menu/index";
import Link from "../../atoms/Link/index";
import Icon from "../../atoms/Icon/index";
import Image from "../../atoms/Image/index";
import Style from "../../../styles/Home.module.css";
export default function Header({ menu }) {
  return (
    <div className="header-area">
      <div className="header-middle">
        <div className="container">
          <div className="row">
            <div className="col-xs-3 col-md-2">
              <div className="logo">
                <Logo
                  href="/"
                  src="https://lab.lepape.com/static/frontend/Lepape/default/fr_FR/images/logo.svg"
                  alt="Lepape équipement Running, Trail, Vélo, Triathlon, Rando, Fitness"
                  imageClassName="img-responsive"
                  height="73"
                  width="200"
                />
              </div>
              <div className={Style.test}>test</div>
            </div>
            <div className="col-xs-9 col-md-7">
              <div className="header-middle-inner">
                <div className="search-container">
                  <form action="#">
                    <div className="search_box">
                      <input
                        className="header-search"
                        placeholder="Rechercher ..."
                        type="text"
                      />
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div className="col-xs-3">
              <div className="box-icone">
                <ul>
                  <li className="settings">
                    <Link href="#?">
                      <Image
                        src="/images/fr.svg"
                        alt="fr"
                        height="26"
                        width="34.2"
                      />
                      <span>France</span>
                    </Link>
                  </li>
                  <li className="settings">
                    <Link href="#?">
                      <Icon className="ico ico-store" />
                      <span>Magasin</span>
                    </Link>
                  </li>
                  <li className="settings">
                    <Link href="#?" title="Se connecter">
                      <Icon className="ico ico-connected" />
                      <span>Se connecter</span>
                    </Link>
                  </li>
                  <li className="settings">
                    <Link href="/checkout/cart" title="Panier">
                      <Icon className="ico ico-basket-full ico-basket" />
                      <span>Panier</span>
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Menu menu={menu} />
    </div>
  );
}
