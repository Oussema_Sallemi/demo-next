import ProductCard from "../../molecules/ProductCard/index";
export default function index({ productsSelections }) {
  const { data, title } = productsSelections;
  const display = data.map((p) => {
    return (
      <div className={p.className} key={p.id}>
        <ProductCard
          imgSrc={p.imgSrc}
          imgClassName={p.imgClassName}
          imgAlt={p.imgAlt}
          width={p.width}
          height={p.height}
          isNew={p.isNew}
          price={p.price}
          href={p.href}
          rating={p.rating}
          product={p.product}
          brand={p.brand}
          key={p.id}
        />
      </div>
    );
  });
  return (
    <div className="container">
      <div className="row">
        <div className="col-lg-12 product-bg">
          <div className="block-title">
            <h2>{title}</h2>
          </div>
          <div className="product-thing slick-custom slick-custom-default">
            <div className="row">{display}</div>
          </div>
        </div>
      </div>
    </div>
  );
}
