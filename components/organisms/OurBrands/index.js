import BrandLogo from "../../molecules/BrandLogo/index";
export default function index({ brands }) {
  const dispaly = brands.map((brand) => {
    return (
      <div className="col-sm-4" key={brand.id}>
        <BrandLogo
          href={brand.href}
          imgSrc={brand.imgSrc}
          imgAlt={brand.imgAlt}
          imgClassName={brand.imgClassName}
          height={brand.height}
          width={brand.width}
          key={brand.id}
        />
      </div>
    );
  });
  return (
    <div className="brand-area mt-15">
      <div className="container">
        <div className="row">
          <h3 className="box-title">NOS MARQUES</h3>
          <div className="col-lg-12 text-center">
            <div className="brand-logo">{dispaly}</div>
          </div>
        </div>
      </div>
    </div>
  );
}
