import Slide from "../../molecules/Slide/index";
export default function index({ slides }) {
  const display = slides.map((slide) => {
    return (
      <Slide
        slideClassName={slides.slideClassName}
        src={slide.src}
        alt={slide.src}
        imgClassName={slide.imgClassName}
        height={slide.height}
        width={slide.width}
        key={slide.src}
      />
    );
  });
  return (
    <div className="slider_section mb-60">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <div className="row slider_area slider-two slider-tools">
              {display}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
