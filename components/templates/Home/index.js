import Head from "next/head";
import Slider from "../../organisms/Slider/index";
import WidgetBloc from "../../organisms/WidgetsBloc/index";
import ProductsSelections from "../../organisms/ProductsSelections/index";
import OurBrands from "../../organisms/OurBrands/index";
import Reassurances from "../../organisms/Reassurances/index";
export default function index({
  title,
  faviconSrc,
  slides,
  widgetsBloc1,
  widgetsBloc2,
  productSelections1,
  productSelections2,
  brands,
  reassurances,
}) {
  return (
    <div className="home">
      <Head>
        <title>{title}</title>
        <link rel="icon" href={faviconSrc} />
        <meta charSet="utf-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge" />
        <meta name="robots" content="noindex, follow" />
        <meta name="description" content="" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
      </Head>
      <Slider slides={slides} />
      <WidgetBloc widgets={widgetsBloc1} />
      <WidgetBloc widgets={widgetsBloc2} />
      <ProductsSelections productsSelections={productSelections1} />
      <ProductsSelections productsSelections={productSelections2} />
      <OurBrands brands={brands} />
      <Reassurances reassurances={reassurances} />
    </div>
  );
}
