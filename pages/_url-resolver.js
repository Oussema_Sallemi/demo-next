import React from "react";
import axios from "axios";
import Product from "./product";
import Category from "./category";

const CONTENT_TYPE = {
  CMS_PAGE: "CMS_PAGE",
  CATEGORY: "CATEGORY",
  PRODUCT: "PRODUCT",
};
export async function getServerSideProps({ query, locale }) {
  const urlKey = query?.pathname;
  if (query.type) {
    return { props: { type: query.type, urlKey, locale } };
  }
  const data = { locale: "fr", url: urlKey[0] };
  const config = {
    method: "post",
    url: "http://localhost:3001/api/url",
    headers: {
      "Content-Type": "application/json",
    },
    data: data,
  };
  try {
    const response = await axios(config);

    const { type } = response.data.data.urlResolver;
    return {
      props: { type, urlKey, locale },
    };
  } catch (error) {
    console.log(error);
  }
}
export default function App({ type, urlKey, locale }) {
  if (type === CONTENT_TYPE.CMS_PAGE) {
    return <div>CMS</div>;
  }

  if (type === CONTENT_TYPE.CATEGORY) {
    return <Category filters={{ url_key: { eq: urlKey, locale } }} />;
  }

  if (type === CONTENT_TYPE.PRODUCT) {
    return <Product filters={{ url_key: { eq: urlKey, locale } }} />;
  }
}
