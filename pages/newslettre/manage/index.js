import { Fragment } from "react";
import Head from "next/head";

export default function Home() {
  return (
    <Fragment>
      <Head>
        <title>Newslettre Subscription</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div>newslettre subscription!</div>
    </Fragment>
  );
}
