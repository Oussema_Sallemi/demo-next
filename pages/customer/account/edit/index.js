import { Fragment } from "react";
import Head from "next/head";

export default function Home() {
  return (
    <Fragment>
      <Head>
        <title>Account Info</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div>customer account edit!</div>
    </Fragment>
  );
}
