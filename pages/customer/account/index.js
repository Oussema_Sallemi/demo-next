import { Fragment } from "react";
import Head from "next/head";

export default function User_Dashboard() {
  return (
    <Fragment>
      <Head>
        <title>User Dashboard</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div>customer account!</div>
    </Fragment>
  );
}
