import HomeTemplate from "../components/templates/Home/index";
export default function Home() {
  const slides = [
    {
      slideClassName: "single_slider",
      src: "/images/banner/ban1.jpg",
      alt: "alt",
      imgClassName: "img-responsive",
      height: "450",
      width: "1364",
    },
  ];
  const widgetsBloc1 = [
    {
      id: 1,
      className: "col-lg-4 col-md-4 col-xs-12 box-home",
      href: "/",
      imgSrc: "/images/widget3.jpg",
      imgAlt: "",
      imgClassName: "",
      height: "267",
      width: "400",
      title: "Chaussures Running / Trail homme",
      detail: "Dynamisme, amorti, protection... Trouvez la chaussure idéale !",
      iconClassName: "ico ico-arrow-right-dot",
    },
    {
      id: 2,
      className: "col-lg-4 col-md-4 col-xs-12 box-home",
      href: "/",
      imgSrc: "/images/widget5.jpg",
      imgAlt: "",
      imgClassName: "",
      height: "267",
      width: "400",
      title: "Chaussures Running / Trail homme",
      detail: "Dynamisme, amorti, protection... Trouvez la chaussure idéale !",
      iconClassName: "ico ico-arrow-right-dot",
    },
    {
      id: 3,
      className: "col-lg-4 col-md-4 col-xs-12 box-home",
      href: "/",
      imgSrc: "/images/widget3.jpg",
      imgAlt: "",
      imgClassName: "",
      height: "267",
      width: "400",
      title: "Chaussures Running / Trail homme",
      detail: "Dynamisme, amorti, protection... Trouvez la chaussure idéale !",
      iconClassName: "ico ico-arrow-right-dot",
    },
  ];
  const widgetsBloc2 = [
    {
      id: 4,
      className: "col-md-6 col-xs-12 box-home big",
      href: "/",
      imgSrc: "/images/widget4.jpg",
      imgAlt: "",
      imgClassName: "img-responsive",
      height: "267",
      width: "400",
      title: "Chaussures Running / Trail homme",
      detail: "Dynamisme, amorti, protection... Trouvez la chaussure idéale !",
      iconClassName: "ico ico-arrow-right-dot",
    },
    {
      id: 5,
      className: "col-md-6 col-xs-12 box-home big",
      href: "/",
      imgSrc: "/images/widget4.jpg",
      imgAlt: "",
      imgClassName: "img-responsive",
      height: "267",
      width: "400",
      title: "Chaussures Running / Trail homme",
      detail: "Dynamisme, amorti, protection... Trouvez la chaussure idéale !",
      iconClassName: "ico ico-arrow-right-dot",
    },
  ];
  const productSelections1 = {
    title: "Test Title 1",
    data: [
      {
        id: 1,
        className: "col-sm-4",
        imgSrc: "/images/img4.jpg",
        imgClassName: "img-responsive",
        imgAlt: "",
        width: 180,
        height: 180,
        isNew: false,
        price: 55.5,
        href: "/",
        rating: 3,
        product: "Test Name!",
      },
      {
        id: 2,
        className: "col-sm-4",
        imgSrc: "/images/img4.jpg",
        imgClassName: "img-responsive",
        imgAlt: "",
        width: 180,
        height: 180,
        isNew: false,
        price: 55.5,
        href: "/",
        rating: 3,
        product: "Test Name!",
      },
      {
        id: 3,
        className: "col-sm-4",
        imgSrc: "/images/img4.jpg",
        imgClassName: "img-responsive",
        imgAlt: "",
        width: 180,
        height: 180,
        isNew: false,
        price: 55.5,
        href: "/",
        rating: 3,
        product: "Test Name!",
      },
    ],
  };
  const productSelections2 = {
    title: "Test Title 2",
    data: [
      {
        id: 4,
        className: "col-sm-4",
        imgSrc: "/images/img6.jpg",
        imgClassName: "img-responsive",
        imgAlt: "",
        width: 180,
        height: 180,
        isNew: true,
        price: 55.5,
        href: "/",
        rating: 4,
        product: "Test Name!",
      },
      {
        id: 5,
        className: "col-sm-4",
        imgSrc: "/images/img7.jpg",
        imgClassName: "img-responsive",
        imgAlt: "",
        width: 180,
        height: 180,
        isNew: true,
        price: 55.5,
        href: "/",
        rating: 4,
        product: "Test Name!",
      },
      {
        id: 6,
        className: "col-sm-4",
        imgSrc: "/images/img7.jpg",
        imgClassName: "img-responsive",
        imgAlt: "",
        width: 180,
        height: 180,
        isNew: true,
        price: 55.5,
        href: "/",
        rating: 4,
        product: "Test Name!",
      },
    ],
  };
  const brands = [
    {
      id: 1,
      href: "/",
      imgSrc: "/images/brand/abus.png",
      imgAlt: "",
      imgClassName: "img-responsive",
      height: "77",
      width: "223",
    },
    {
      id: 2,
      href: "/",
      imgSrc: "/images/brand/2xu_1.png",
      imgAlt: "",
      imgClassName: "img-responsive",
      height: "77",
      width: "223",
    },
    {
      id: 3,
      href: "/",
      imgSrc: "/images/brand/adidas.png",
      imgAlt: "",
      imgClassName: "img-responsive",
      height: "77",
      width: "223",
    },
  ];
  const reassurances = {
    title: "NOS GARANTIES & SERVICES",
    data: [
      {
        id: 1,
        title: "Stock en temps réel",
        iconClassName: "ico ico-real-time-stock",
        text: "parmis plus de 35 000 références",
        className: "col-sm-2",
      },
      {
        id: 2,
        title: "Testez & commandez le en magasin",
        iconClassName: "ico ico-store",
        text: "A Paris (8e arr.) et Lyon (2e arr.)",
        className: "col-sm-2",
      },
      {
        id: 3,
        title: "Livraison offerte en France métropolitaine",
        iconClassName: "ico ico-delivery-offer",
        text: "en France métropolitaine",
        className: "col-sm-2",
      },
      {
        id: 4,
        title: "Etude posturale et réglages offerts",
        iconClassName: "ico ico-bike-adjustement",
        text: "dans nos ateliers de Paris et Lyon",
        className: "col-sm-2",
      },
      {
        id: 5,
        title: "Garantie 2 ans",
        iconClassName: "ico ico-2y-warranty",
        text: "SAV assuré partout en France",
        className: "col-sm-2",
      },
      {
        id: 6,
        title: "Paiement CB sécurisé",
        iconClassName: "ico ico-payment-card-234",
        text: "en 2 X sans frais",
        className: "col-sm-2",
      },
    ],
  };
  return (
    <HomeTemplate
      slides={slides}
      title="Lepape Demo | V0"
      faviconSrc="/favicon.ico"
      widgetsBloc1={widgetsBloc1}
      widgetsBloc2={widgetsBloc2}
      productSelections1={productSelections1}
      productSelections2={productSelections2}
      brands={brands}
      reassurances={reassurances}
    />
  );
}
