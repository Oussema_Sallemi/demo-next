import { Fragment } from "react";
import Head from "next/head";

export default function Home() {
  return (
    <Fragment>
      <Head>
        <title>My Orders</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div>customer orders!</div>
    </Fragment>
  );
}
