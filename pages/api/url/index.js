const axios = require("axios");
export default async (req, res) => {
  var data = JSON.stringify({
    query: `query ($url:String!){
  urlResolver(url: $url) {
    entity_uid
    relative_url
    redirectCode
    type
  }
}`,
    variables: { url: req.body.url },
  });

  const stores = [
    { store_code: "de", locale: "de-de" },
    { store_code: "de", locale: "de-ch" },
    { store_code: "de", locale: "de-gb" },
    { store_code: "de", locale: "de-lu" },
    { store_code: "en", locale: "en" },
    { store_code: "es", locale: "es-es" },
    { store_code: "default", locale: "fr" },
    {
      store_code: "default",
      locale: "fr-be",
    },
    {
      store_code: "default",
      locale: "fr-ch",
    },
    {
      store_code: "default",
      locale: "fr-lu",
    },
    { store_code: "it", locale: "it-ch" },
    { store_code: "it", locale: "it-it" },
    { store_code: "nl", locale: "nl-de" },
    { store_code: "nl", locale: "nl-nl" },
    { store_code: "pt", locale: "pt-pt" },
  ];

  try {
    const s = stores.filter((store) => {
      if (store.locale === req.body.locale) {
        return store;
      }
    });
    const response = await axios({
      method: "post",
      url: "https://lab.lepape.com/graphql",
      headers: {
        Authorization: "Bearer oopm3p8oj048f63oudvfxpoz55nyczi0",
        "Content-Type": "application/json",
        Cookie:
          "PHPSESSID=bbgm15u1fsssdco4llqa1v85mi; private_content_version=4d0d6e811352c14eb3aba1cfc5562b49",
        Store: s[0].store_code,
      },
      data: data,
    });
    res.json(response.data);
  } catch (error) {
    console.log(error);
  }
};
