const axios = require("axios");
export default async (req, res) => {
  const data = JSON.stringify({
    query: `{
  categories(filters: {
    ids: {in: ["2020","2033","2056","2365","2392","2078", "2117","2154","2271","2212","2249","6878"]}
    parent_id: { in: ["2019"]
  } }) {
    items {
      id
      name
      url_key
      menu_name
    }
  }
}`,
    variables: {},
  });

  const stores = [
    { store_code: "de", locale: "de-de" },
    { store_code: "de", locale: "de-ch" },
    { store_code: "de", locale: "de-gb" },
    { store_code: "de", locale: "de-lu" },
    { store_code: "en", locale: "en" },
    { store_code: "es", locale: "es-es" },
    { store_code: "default", locale: "fr" },
    {
      store_code: "default",
      locale: "fr-be",
    },
    {
      store_code: "default",
      locale: "fr-ch",
    },
    {
      store_code: "default",
      locale: "fr-lu",
    },
    { store_code: "it", locale: "it-ch" },
    { store_code: "it", locale: "it-it" },
    { store_code: "nl", locale: "nl-de" },
    { store_code: "nl", locale: "nl-nl" },
    { store_code: "pt", locale: "pt-pt" },
  ];

  try {
    const s = stores.filter((store) => store.locale === req.query.locale);
    const response = await axios({
      method: "post",
      url: "https://lab.lepape.com/graphql",
      headers: {
        Authorization: "Bearer oopm3p8oj048f63oudvfxpoz55nyczi0",
        "Content-Type": "application/json",
        Cookie:
          "PHPSESSID=bbgm15u1fsssdco4llqa1v85mi; private_content_version=4d0d6e811352c14eb3aba1cfc5562b49",
        Store: s[0].store_code,
      },
      data: data,
    });
    const {
      data: {
        categories: { items },
      },
    } = response.data;
    res.json(items);
  } catch (error) {
    console.log(error);
  }
};
