import { Provider } from "../context";

import Head from "next/head";
import Main from "../layouts/Main";

import "../styles/font-awesome.min.css";
import "../styles/plugins.css";
import "../styles/style.css";

const MyApp = ({ Component, pageProps }) => {
  return (
    <Provider>
      <Main>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
        </Head>
        <Component {...pageProps} />
      </Main>
    </Provider>
  );
};
export default MyApp;
