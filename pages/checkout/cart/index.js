import React from "react";
import Head from "next/head";

export default function index() {
  return (
    <React.Fragment>
      <Head>
        <title>Panier</title>
        <link rel="icon" href="/favicon.ico" />
        <meta charSet="utf-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge" />
        <meta name="robots" content="noindex, follow" />
        <meta name="description" content="" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
      </Head>
      <div>Cart!</div>
    </React.Fragment>
  );
}
