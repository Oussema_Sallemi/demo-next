const nextTranslate = require("next-translate");
module.exports = {
  images: {
    domains: ["lab.lepape.com"],
  },
  async rewrites() {
    return [
      {
        source: "/:pathname*",
        destination: "/_url-resolver",
      },
    ];
  },
  ...nextTranslate(),
};
